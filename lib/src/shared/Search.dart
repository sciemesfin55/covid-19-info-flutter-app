import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.7,
      decoration: BoxDecoration(
        //color: Color(0xfff5f8fd),
        color: Colors.grey[200],
        borderRadius: BorderRadius.circular(30),
      ),
      margin: EdgeInsets.only(top: 10, bottom: 10),
      padding: EdgeInsets.only(left: 20, right: 10),
      child: Row(
        children: <Widget>[
          InkWell(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.only(right: 7),
              child: Icon(
                Icons.search,
                color: Colors.grey,
              ),
            ),
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Search eesand", border: InputBorder.none),
            ),
          ),
        ],
      ),
    );
  }
}

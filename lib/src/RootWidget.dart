import 'package:cinfo/src/bloc/phone_no/phone_no_bloc.dart';
import 'package:cinfo/src/bloc/phone_no/phone_no_provider.dart';
import 'package:cinfo/src/shared/Loading.dart';
import 'package:cinfo/src/widgets/Home.dart';
import 'package:cinfo/src/widgets/user/Signin.dart';
import 'package:flutter/material.dart';

class RootWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PhoneNoBloc auth = PhoneNoBlocProvider.of(context);
    return StreamBuilder<String>(
      stream: auth.onAuthStateChanged,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.active ||
            snapshot.connectionState == ConnectionState.done) {
          final bool isLoggedIn = snapshot.hasData;
          return isLoggedIn ? Home() : Signin();
        }
        return _buildWaitingScreen();
      },
    );
  }

  Widget _buildWaitingScreen() {
    return SafeArea(
      child: Scaffold(
        body: Loading(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.school, color: Colors.green),
              Container(
                  padding: EdgeInsets.only(left: 5),
                  child: Text('Covid-19 Info',
                      style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontSize: 20))),
            ],
          ),
        ),
      ),
    );
  }
}

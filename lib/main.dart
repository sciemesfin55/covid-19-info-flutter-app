import 'package:cinfo/src/bloc/phone_no/phone_no_provider.dart';
import 'package:flutter/material.dart';
import 'package:cinfo/src/RootWidget.dart';

void main() {
  runApp(Covid19Info());
}

class Covid19Info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PhoneNoBlocProvider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: RootWidget(),
      ),
    );
  }
}
